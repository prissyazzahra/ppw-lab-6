## Repository

Prissy Azzahra Ratnadwita - 1706984700

## Status Aplikasi

[![Pipeline](https://gitlab.com/prissyazzahra/ppw-lab-6/badges/master/pipeline.svg)](https://gitlab.com/prissyazzahra/ppw-lab-6/commits/master)

[![Coverage](https://gitlab.com/prissyazzahra/ppw-lab-6/badges/master/coverage.svg)](https://gitlab.com/prissyazzahra/ppw-lab-6/commits/master)



## Link Heroku App

http://prissy-lab6.herokuapp.com/
