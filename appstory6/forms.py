from django import forms
from .models import Status
title_attrs = {
     'type': 'text',
     'class': 'status-form-input',
     'placeholder':'Tell me how you feel!'
 }

class StatusForm(forms.Form):
	status = forms.CharField()