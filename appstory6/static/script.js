$(document).ready(function() {
	$("#darkmode-switch").click(function() {
		if(this.checked){
          $('link[href="/static/style.css"]').attr('href', '/static/style2.css');
        } else {
          $('link[href="/static/style2.css"]').attr('href', '/static/style.css');
        }
	});

	var acc = document.getElementsByClassName("accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
	  acc[i].addEventListener("click", function() {
	    this.classList.toggle("active");
	    var panel = this.nextElementSibling;
	    if (panel.style.maxHeight){
	      panel.style.maxHeight = null;
	    } else {
	      panel.style.maxHeight = panel.scrollHeight + "px";
	    } 
	  });
	}
});

