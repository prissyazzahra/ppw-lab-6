from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
from .models import *
from .forms import *
import time
# Create your tests here.

class Lab6_Test(TestCase):
	def test_lab_6_url_is_exist(self):
		response = Client().get('/home/')
		self.assertEqual(response.status_code, 200)

	def test_lab_6_url_404_error(self):
		response = Client().get('/wek/')
		self.assertEqual(response.status_code, 404)

	def test_lab6_using_index_func(self):
		found = resolve('/home/')
		self.assertEqual(found.func, home, deleteStatuses)

	def test_lab6_redirects_to_home(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 302)

	def test_lab_6_has_hello_apa_kabar(self):
		response = Client().get('/home/')
		html_response = response.content.decode('utf8')
		self.assertIn("Hello, Apa kabar?", html_response)

	def test_form_validation_for_blank_items(self):
		form = StatusForm(data={'status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['status'],
			["This field is required."]
		)

	def test_lab_6_has_status_input(self):
		form = StatusForm()
		self.assertIn('id="id_status"', form.as_p())

	def test_lab6_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/home/', {'status': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/home/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_lab6_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/home/', {'status': ''})
		self.assertEqual(response_post.status_code, 200)

		response= Client().get('/home/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

	def test_lab6_profile_page_is_exist(self):
		response = Client().get('/profile/')
		self.assertEqual(response.status_code, 200)

	def test_lab6_profile_has_name(self):
		response = Client().get('/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn("Prissy Azzahra Ratnadwita", html_response)

	def test_lab6_profile_has_photo(self):
		response = Client().get('/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn('img', html_response)

	def test_lab6_profile_has_description(self):
		response = Client().get('/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn('class="description', html_response)

	def test_lab6_delete_function_works(self):
		response = Client().get('/delete/')
		self.assertEqual(response.status_code, 302)

class Lab7FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Lab7FunctionalTest, self).setUp()

	def test_input_status(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://prissy-lab6.herokuapp.com/home/')
		# find the form element
		status = selenium.find_element_by_id('id_status')
		submit = selenium.find_element_by_id('submit')

		# Fill the form with data
		status.send_keys("Coba Coba")

		# submitting the form
		submit.send_keys(Keys.RETURN)
		time.sleep(5)
		self.assertIn("Coba Coba", selenium.page_source)

	def test_profile_link_in_home(self):
		selenium = self.selenium
		selenium.get('http://prissy-lab6.herokuapp.com/home/')
		profile = selenium.find_element_by_link_text('Profile').text
		time.sleep(5)
		self.assertIn(profile, selenium.page_source)

	def test_name_in_profile(self):
		selenium = self.selenium
		selenium.get('http://prissy-lab6.herokuapp.com/profile/')
		header2 = selenium.find_element_by_tag_name('h2').text
		time.sleep(5)
		self.assertIn('Prissy Azzahra Ratnadwita', header2)

	def test_profile_has_theme_change_func(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/profile')
		switch = selenium.find_element_by_css_selector('label.switch')
		time.sleep(10)
		switch.click()
		css_find = selenium.find_element_by_css_selector('h2')
		css_property = css_find.value_of_css_property('color')
		time.sleep(5)
		self.assertEqual("rgba(255, 255, 255, 1)", css_property)

	def test_link_has_style(self):
		selenium = self.selenium
		selenium.get('http://prissy-lab6.herokuapp.com/home/')
		link = selenium.find_element_by_css_selector('a')
		css_property = link.value_of_css_property('color')
		time.sleep(5)
		self.assertEqual("rgba(255, 255, 255, 1)", css_property)

	def test_profile_has_style(self):
		selenium = self.selenium
		selenium.get('http://prissy-lab6.herokuapp.com/profile/')
		foto = selenium.find_element_by_id('foto')
		css_property = foto.value_of_css_property('border-radius')
		time.sleep(5)
		self.assertEqual("50%", css_property)

	def tearDown(self):
		self.selenium.quit()
		super(Lab7FunctionalTest, self).tearDown()
