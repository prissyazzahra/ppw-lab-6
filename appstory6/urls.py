from django.conf.urls import url, path
from . import views

urlpatterns = [
	path('', home, name="home")
	path('', profile, name="profile")
	path('', deleteStatuses, name="deleteStatuses")
	# path('', poststatus, name="poststatus")
	# path('', showstatus, name="showstatus")
]