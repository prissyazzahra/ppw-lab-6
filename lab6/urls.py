"""lab6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views
from appstory6.views import home, redirecting, profile, deleteStatuses
from story9.views import library, jsonData, logout_view, addFav, removeFav
from story10.views import registration, validate, success

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', redirecting, name="redirecting"),
    path('home/', home, name="home"),
    path('profile/', profile, name="profile"),
    path('delete/', deleteStatuses, name="delete"),
    path('library/', include('story9.urls')),
    path('data/', jsonData, name="data"),
    path('login/', views.LoginView.as_view(), name="login"),
    path('logout/', logout_view , name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
    path('', include('story10.urls')),
]
