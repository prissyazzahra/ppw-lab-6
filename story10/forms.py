from django import forms
from .models import SubModel

class Subs(forms.Form):
	name = forms.CharField(widget=forms.TextInput(attrs={'class':'forms', 'required': True, 'placeholder': 'John Doe'}), label='Name',error_messages={"required": "Please enter your name"}, max_length=400)
	pswrd = forms.CharField(widget=forms.PasswordInput(attrs={'class':'forms', 'required': True, 'placeholder': 'Password'}), label='Password', min_length=8, max_length=20, error_messages={"required": "Please enter a password between 8 to 20 characters long."})
	email = forms.EmailField(widget=forms.TextInput(attrs={'class':'forms', 'required': True, 'placeholder': 'johndoe@example.com'}), label='E-mail', max_length=30, error_messages={"required": "Please enter a valid e-mail."})
