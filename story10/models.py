from django.db import models

# Create your models here.

class SubModel(models.Model):
	name = models.CharField(max_length=400)
	pswrd = models.CharField(max_length=20)
	email = models.EmailField(max_length=30)
