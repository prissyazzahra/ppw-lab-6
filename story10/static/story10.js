$(document).ready(function() {
    var name = "";
    var pswrd = "";
    $("#id_name").change(function() {
        name = $(this).val();
    });
    $("#id_pswrd").change(function() {
        pswrd = $(this).val();
    })
    $("#id_email").change(function() {
        var email = $(this).val();
        $.ajax({
            url: "validate/",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data)
                if(data.not_valid) {
                    alert("This email is already taken");
                } else {
                    if(name.length > 0 && pswrd.length > 8) {
                        $("#submit_subscribe").removeAttr("disabled");
                    }
                }
            }
        });
    });
    $("#submit_subscribe").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"success/",
            data: $("form").serialize(),
            method: 'POST',
            dataType: 'json',
            success: function(data){
                alert("Your response has been recorded!")
                window.location.replace("/profile/");
            }, error: function() {
                alert("Error, cannot get data from server")
            }
        })
    });
    $.ajax({
        url: 'success/',
        success: function(result) {
            var hdr = "<thead><tr><th>Name</th><th>Email</th><th> </th></tr></thead>";
            $('#users').append(hdr);
            $('#users').append('<tbody>');
            var res = JSON.parse(result)
            console.log(res[1])
            for (i=0; i<res.length; i++) {
                var tmp = "<tr><td>" + res[i].fields.name + "</td><td>" + res[i].fields.email
                + "</td><td>" + "<a href=" + "'javascript:void(0)' onclick='deleteSub(" + res[i].pk + ")'>Unsubscribe</a>";
                $('#users').append(tmp);
            }
            $('#users').append('</tbody>');
        }
    })
});

function deleteSub(pk){
        console.log(pk)
        $.ajax({
            method:"POST",
            url: "delete/",
            data:{'pk':pk},
            success:function(){
                alert("Unsubscribed from my site! :(")
                window.location.reload();
            }
        })
    }