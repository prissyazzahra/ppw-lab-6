from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import SubModel
from .forms import Subs
from django.utils.encoding import force_text

# Create your tests here.
class Lab10_Test(TestCase):
	def test_lab_10_registration_exists(self):
		response = Client().get('/registration/')
		self.assertEqual(response.status_code, 200)

	def test_lab_10_can_create_subscriber_object(self):
		SubModel.objects.create(name="John Doe", pswrd="johndoeisme", email="johndoe@gmail.com")
		count_sub = SubModel.objects.all().count()
		self.assertEqual(count_sub, 1)

	def test_lab_10_check_for_blank_form(self):
		form = Subs(data={'name': '', 'pswrd': '', 'email':'' })
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['name'],
			["Please enter your name"]
		)
		self.assertEqual(
			form.errors['pswrd'],
			["Please enter a password between 8 to 20 characters long."]
		)
		self.assertEqual(
			form.errors['email'],
			["Please enter a valid e-mail."]
		)

	def test_lab_10_check_for_invalid_pswrd(self):
		form = Subs(data={'name': 'mama', 'pswrd': 'plo', 'email':'mama@momo.com' })
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['pswrd'],
			['Ensure this value has at least 8 characters (it has 3).']
		)

	# def test_double_subscriber(self):
	# 	name = 'John Doe'
	# 	pswrd = 'johndoeisyou'
	# 	email = 'johndoe@gmail.com'
	# 	go = Client().post('/validate/', {'email': email})
	# 	# must respond false
	# 	self.assertJSONEqual(
	# 		force_text(go.content),
	# 		{'not_valid': False}
	# 	)
	# 	# button not disabled, send data to views
	# 	Client().post('/success', {'name': name, 'pswrd': pswrd, 'email': email})
	# 	subscriber_count = Subscriber.objects.all().count()
	# 	self.assertEqual(subscriber_count, 1)
	# 	# email used second time
	# 	go = Client().post('/validate', {'email': email})
	# 	# must respond true, email already taken
	# 	self.assertJSONEqual(
	# 		str(go.content, encoding='utf8'),
	# 		{'not_valid': True}
	# 	)
	# 	# not making any new data
	# 	subscriber_count = Subscriber.objects.all().count()
	# 	self.assertEqual(subscriber_count, 1)

