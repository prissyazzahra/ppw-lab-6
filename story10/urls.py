from django.contrib import admin
from django.urls import path
from .views import registration, success, validate, delete


urlpatterns = [
    path('registration/', registration, name='registration'),
    path('registration/success/', success, name="success"),
    path('registration/validate/', validate, name="validate"),
    path('registration/delete/', delete, name="delete")
]
