from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .forms import Subs
from django.core import serializers
from .models import SubModel
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.

def registration(request):
	response = {}
	users = SubModel.objects.all()
	response['forms'] = Subs()
	response['users'] = users
	return render(request, 'regist.html', response)

@csrf_exempt
def validate(request):
	email = request.POST.get("email")
	data = {'not_valid': SubModel.objects.filter(email__iexact=email).exists()}
	return JsonResponse(data)

def success(request):
	submit = Subs(request.POST or None)
	data = {}
	if (request.method == "POST" and submit.is_valid()):
		cd = submit.cleaned_data
		new_subscriber = SubModel(name=cd['name'], pswrd=cd['pswrd'], email=cd['email'])
		new_subscriber.save()
		data = {'name': cd['name'], 'pswrd': cd['pswrd'], 'email': cd['email']}
	elif (request.method == "GET"):
		obj = SubModel.objects.all()
		data = serializers.serialize('json', obj)
	return JsonResponse(data, safe=False)

@csrf_exempt
def delete(request):
	if (request.method == "POST"):
		subId = request.POST['pk']
		SubModel.objects.filter(pk=subId).delete()
		return HttpResponse(json.dumps({'message': "Succeed"}),content_type="application/json")
	else:
		return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),content_type="application/json")

