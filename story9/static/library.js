function search(arg)  {

  $(function() {
	$.ajax({
		url: '/data/?cari=' + arg,
		success: function(result) {
			result = result.items
			var hdr = "<thead><tr><th>Title</th><th>Author</th><th>Thumbnail</th><th>Description</th><th></th></tr></thead>";
			$('#book').append(hdr);
			$('#book').append('<tbody>');
			for (i=0; i<result.length; i++) {
				var tmp = "<tr><td>" + result[i].volumeInfo.title + "</td><td>" + result[i].volumeInfo.authors + "</td><td>" +
				"<img src='"+result[i].volumeInfo.imageLinks.thumbnail+ "'>"+"</td><td>" + result[i].volumeInfo.description +
				"</td><td>"+"<button class='button' style='background-color:transparent; border:none' id='"+ result[i].id +
				"' onclick ='addFavorite(" + "\"" + result[i].id + "\"" + ")'><span class='fa fa-star' style='color:gray'></span></button>"
				+ "</td></tr>";
				$('#book').append(tmp);
			}
			$('#book').append('</tbody>');
		}
	})
  });
}

var count=0;
function addFavorite(id) {
	var star = $('#' + id).html();
	if(star.includes("gray")) {
        $.ajax({
    		url: "/library/add",
    		dataType: 'json',
    		success: function(result) {
				var count = JSON.parse(result);
				console.log(result)
		        $('#' + id).html("<span style='color: #F9F871;' class='fa fa-star'></span>");
		        $('#fav').html("<span style='color: #F9F871;' class='fa fa-star'></span>" + count + " ");
			}
		});
	}
    else{
        $.ajax({
        	url: "/library/remove",
    		dataType: 'json',
    		success: function(result) {
				var count = JSON.parse(result);
				console.log(result)
		        $('#'+id).html("<span style='color: gray;' class='fa fa-star'></span>");
		        $("#fav").html("<span style='color: #F9F871;' class='fa fa-star'></span>" + count + " ");
			}
   		 });
	}
}

$(function() {
	$('#comics').click(function() {
		$('#book thead').remove();
		$('#book tbody').remove();
		count=0;
		$('#fav').html("<span style='color: #F9F871;' class='fa fa-star'></span>" + count);
		search('comic')
	});

	$('#arch').click(function() {
		$('#book thead').remove();
		$('#book tbody').remove();
		count=0;
		$('#fav').html("<span style='color: #F9F871;' class='fa fa-star'></span>" + count);
		search('architecture')
	});

	$('#quilting').click(function() {
		$('#book thead').remove();
		$('#book tbody').remove();
		count=0;
		$('#fav').html("<span style='color: #F9F871;' class='fa fa-star'></span>" + count);
		search('quilting')
	});
});

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}

function signOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function () {
		console.log('User signed out.');
	});
}
