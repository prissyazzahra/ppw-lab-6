from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from django.contrib.auth.models import User

# Create your tests here.

class Lab9_Test(TestCase):
	def test_lab9_uses_library_as_template(self):
		response = Client().get('/library/')
		self.assertTemplateUsed(response, 'library.html')

	def test_lab9_url_exists(self):
		response = Client().get('/library/')
		self.assertEqual(response.status_code, 200)

	def test_json_data_url_exists(self):
		response = Client().get('/data/?cari=comic')
		self.assertEqual(response.status_code, 200)

	def test_lab11_has_add_func(self):
		found = resolve('/library/add/')
		self.assertEqual(found.func, addFav)

	def test_lab11_has_remove_func(self):
		found = resolve('/library/remove/')
		self.assertEqual(found.func, removeFav)

	def test_user_login(self):
		 User.objects.create_superuser('admin', 'foo@foo.com', 'admin')
		 self.client.login(username='admin', password='admin')
		 session = self.client.session
		 self.assertEqual(session['username'], 'admin')

	def test_lab11_has_count(self):
		found = resolve('/library/add/')
		session = self.client.session
		self.assertEqual(session['count'], 0)
