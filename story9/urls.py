from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
	path('', library, name="library"),
	path('', jsonData, name="jsonData"),
	path('add/', addFav, name="add"),
	path('remove/', removeFav, name="remove"),
]