from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login, logout
import requests
import json

# Create your views here.

def library(request):
	if request.user.is_authenticated:
		request.session['first_name'] = request.user.first_name
		request.session['last_name'] = request.user.last_name
		request.session['email'] = request.user.email
		count = request.session.get('count', 0)
		print(dict(request.session))
		for key, value in request.session.items():
			print('{} => {}'.format(key, value))
	return render(request, 'library.html')

def jsonData(request):
	searching =  request.GET.get('cari')
	url = "https://www.googleapis.com/books/v1/volumes?q=" + searching
	data = requests.get(url).json()
	if request.user.is_authenticated:
		count = request.session.get('count', 0)
		request.session['count'] = count
		for key, value in request.session.items():
			print('{} => {}'.format(key, value))
	return JsonResponse(data)

def addFav(request):
	print(dict(request.session))
	request.session['count'] = request.session['count'] + 1
	return HttpResponse(request.session['count'], content_type = 'application/json')

def removeFav(request):
	request.session['count'] = request.session['count'] - 1
	return HttpResponse(request.session['count'], content_type = 'application/json')

def logout_view(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/library')

